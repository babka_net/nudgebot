from .core import create_mastodon
from collections import defaultdict

m = create_mastodon()

def find_new_accounts(): # TODO: Add a max_id argument
    flagged = defaultdict(set)
    profiles = m.admin_accounts()
    while profiles:
        for profile in profiles:
            profile = profile['account']
            username = profile['username']
            if not profile['statuses_count']:
                flagged[username].add("0_STATUS_COUNT")
            if not profile['note']:
                flagged[username].add("EMPTY_DESCRIPTION")
            if not profile['following_count']:
                flagged[username].add("0_FOLLOWING_COUNT")
            if not profile['discoverable']:
                flagged[username].add("NOT_DISCOVERABLE")
        profiles = m.admin_accounts(max_id = profile['id'])
    return flagged


import argparse
from .actions import find_new_accounts

def show_accounts(): # TODO: Come up with a better name
    accounts = find_new_accounts()
    for account,reasons in accounts.items():
        print(f"{account}: {', '.join(reasons)}")


from os import environ as env
from dotenv import load_dotenv
from redis import Redis
from mastodon import Mastodon

load_dotenv()  # Get environment variables from .env

### Let's write some helper functions that use our environment properly
def create_redis(redis_url=None):
    """Create a Redis DB connection"""
    if not redis_url:
        redis_url = env.get('REDIS_URL') or 'redis://localhost:6379/0'
    redis_client = Redis.from_url(redis_url)
    return redis_client

def create_mastodon(api_base_url = None, access_token = None):
    if not access_token:
        access_token = env['MASTODON_ACCESS_TOKEN']
    if not api_base_url:
        api_base_url = env['MASTODON_BASE_URL']
    mastodon_client = Mastodon(
        access_token =  access_token,
        api_base_url = api_base_url,
        ratelimit_method='wait', 
    )
    return mastodon_client
